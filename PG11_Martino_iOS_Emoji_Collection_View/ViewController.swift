//
//  ViewController.swift
//  PG11_Martino_iOS_Emoji_Collection_View
//
//  Created by Martino Kang on 2018-03-11.
//  Copyright © 2018 Martino Kang. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var m_CollectionView: UICollectionView!
    
    var m_EmojiArray = ["😀", "😃", "😄", "😁", "😆", "😅", "😂", "🤣", "😃", "😄", "😁", "😆", "😅", "😂", "🤣", "😃", "😄", "😁", "😆", "😅", "😂", "🤣", "😃", "😄", "😁", "😆", "😅", "😂", "🤣", "😃", "😄", "😁", "😆", "😅", "😂", "🤣", "😃", "😄", "😁", "😆", "😅", "😂", "🤣", "😃", "😄", "😁", "😆", "😅", "😂", "🤣", "😃", "😄", "😁", "😆", "😅", "😂", "🤣"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        m_CollectionView.register(UINib.init(nibName: "CustomCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "emojiCell")
        
        m_CollectionView.dataSource = self

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension ViewController:UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = m_CollectionView.dequeueReusableCell(withReuseIdentifier: "emojiCell", for: indexPath) as! CustomCollectionViewCell
        
        cell.m_Label.text = m_EmojiArray[indexPath.row]
        
        return cell

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return m_EmojiArray.count
    }
}



